export default function handler(req, res) {
   // const client_id = '16obGOHAAM_1xRM_60nB';
   const client_id= process.env.NAVER_LOGIN_CLIENTID;
    const state = "RANDOM_STATE";
    const redirectURI = encodeURI("http://localhost:3000/naverLoginCallback");
    const api_url = `https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=${client_id}&redirect_uri=${redirectURI}&state=${state}`;
    
    res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' });
    res.end(`<a href="${api_url}"><img height="50" src="http://static.nid.naver.com/oauth/small_g_in.PNG"/></a>`);
  }