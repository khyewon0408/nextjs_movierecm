const axios = require('axios');

const token = "YOUR_ACCESS_TOKEN";
const header = "Bearer " + token;
console.log('회원정보 ');
axios.get('https://openapi.naver.com/v1/nid/me', {
    headers: {
        'Authorization': header
    }
})
.then(response => {
    console.log(response.data);
})
.catch(error => {
    console.error(error);
});