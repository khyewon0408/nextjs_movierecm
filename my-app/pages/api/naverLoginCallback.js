import axios from 'axios';
import { setCookie, parseCookies } from 'nookies';

export default async function handler(req, res) {
  console.log('들어오나??');
  const client_id = process.env.NAVER_LOGIN_CLIENTID;
  const client_secret = process.env.NAVER_LOGIN_CLIENT_SECRET;
  let state = "RANDOM_STATE";
console.log('req' , req);
  const { cookies } = parseCookies( req );

  // 콜백 처리 로직
  const code = req.query.code;
  state = req.query.state;
  const redirectURI = encodeURI("http://localhost:3000");
  const api_url = `https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=${client_id}&client_secret=${client_secret}&redirect_uri=${redirectURI}&code=${code}&state=${state}`;

  try {
    const response = await axios.get(api_url, {
      headers: { 'X-Naver-Client-Id': client_id, 'X-Naver-Client-Secret': client_secret }
    });

    const responseData = response.data;
    const accessToken = responseData.access_token;

    // 다른 API 호출
    const header = { 'Authorization': `Bearer ${accessToken}` };
    const otherResponse = await axios.get('https://openapi.naver.com/v1/nid/me', {
      headers: header
    });

    // 쿠키 설정
    setCookie(null, 'userInfoData', otherResponse.data, {
      maxAge: 3600, // 쿠키의 유효 기간 설정 (초 단위, 여기서는 1시간)
      path: '/', // 쿠키의 유효 경로 설정
      httpOnly: true, // 클라이언트에서 쿠키에 접근할 수 없도록 설정
    });

    console.log('쿠키쿠키::: ' ,  cookies);

    // 다른 페이지로 리다이렉트 (예: '/new-page'로 설정)
    res.writeHead(302, { Location: 'http://localhost:3000' });
    res.end();
  } catch (error) {
    console.error('Error:', error);
    res.status(500).end();
  }
} 