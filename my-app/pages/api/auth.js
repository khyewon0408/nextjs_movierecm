import { cookies } from 'next-cookies';
export async function getServerSideProps(context) {
    const { req, res, query, cookies } = context;
  
    // ... 코드
  
    const code = query.code;
    const userInfoData = await getOtherData(code);
  
    const cookieStore = cookies(req);
    cookieStore.set('userInfoData', userInfoData, {
      httpOnly: true,
      path: '/',
    });
  
    return { 
      props: {
        userInfoData
      },
    };
  }