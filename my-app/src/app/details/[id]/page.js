"use client"
import { useParams } from "next/navigation"
import { useEffect, useState } from "react";

export default function getDetails(){
    const params =  useParams();
    const movieId = params.id;
    const [detailData,setDatailData] = useState(null);
     useEffect(()=>{
        const asyncData = async()=>{
        const options = {
            method: 'GET',
            headers: {
              accept: 'application/json',
              Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0NzU4YWQ3OTUxZWY5NGI5ZmE0MTc3YWVkYmZjNDk3OSIsInN1YiI6IjY1YzE3ZGI5ZjQ0ZjI3MDE4NGMwMWU0OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HLXkiPgNGR8dkYzKSSAHAKOZcPVWfI_Md67-z1qiZhY'
            }
          };
          
        const response =   await  fetch(`https://api.themoviedb.org/3/movie/${movieId}?language=ko-KR&include_adult=false`, options); 
        const data = await response.json();
        setDatailData(data);
     
        }
        asyncData();


     },[movieId]);

     useEffect(()=>{
        console.log('데이터 확인 ' ,  detailData);
     },[detailData]);

     if (!detailData) {
        return <p>Loading...</p>;
      }
    
      return (
        <div>
          <img src={`https://image.tmdb.org/t/p/w185${detailData.poster_path}`} alt={detailData.title} />
          <h1>{detailData.title}</h1>
          <p>{detailData.overview}</p>
          <p>평가 개수: {detailData.vote_count}</p>
          <p>평점: {detailData.vote_average}</p>
          <p>런타임: {detailData.runtime} 분</p>
          <p>한줄태그: {detailData.tagline}</p>
          <p>장르: {detailData.genres.map(genre => genre.name).join(', ')}</p>
        </div>
      );
     
};