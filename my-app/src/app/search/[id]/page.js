"use client"
import { useState, useEffect } from 'react';
import { useRouter} from 'next/navigation';
const MyComponent = ({ params }) => {
  const [topic, setTopic] = useState(null);
  const router = useRouter();
  useEffect(() => {
    const fetchData = async () => {
      const options = {
        method: 'GET',
        headers: {
          accept: 'application/json',
          Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0NzU4YWQ3OTUxZWY5NGI5ZmE0MTc3YWVkYmZjNDk3OSIsInN1YiI6IjY1YzE3ZGI5ZjQ0ZjI3MDE4NGMwMWU0OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HLXkiPgNGR8dkYzKSSAHAKOZcPVWfI_Md67-z1qiZhY'
        }
      };

      try {
        const response = await fetch(`https://api.themoviedb.org/3/search/movie?include_adult=false&include_video=false&language=ko-KR&query=${params.id}&page=1`, options);
        const data = await response.json();
        console.log("검색 결과 :::  " ,  data)
        setTopic(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [params.id]);

  return (
    <div>
      {topic ? (
        <div>
          <h1>SEARCH LIST</h1>
          <ul>
            {topic.results.length > 0 ? (
              topic.results.map((item, index) => (
                <li key={index}>
                  {process.env.TMDB_IMG_URL}
                  <img src={'https://image.tmdb.org/t/p/w185' + item.poster_path} alt="Movie Poster" 
                  onClick={() => {
                    router.push('/details/' + item.id);}}/>
                  <p>{(index + 1) + '. ' + item.original_title}</p>
                  <p>{item.overview}</p>
                </li>
              ))
            ) : (
              <p>결과가 없습니다.</p>
            )}
          </ul>
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};
export default MyComponent;