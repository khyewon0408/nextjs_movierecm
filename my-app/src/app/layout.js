"use client"
import { useRouter ,usePathname ,useParams} from 'next/navigation';
import GetHomeRecmList from './comp/homeRecmList';
import Header from './comp/header';
import LoginHeader from './comp/loginHeader';

const RootLayout = ({ children }) => {
  const router = useRouter();
  const pathname  = usePathname();

  const params = useParams();
  const pageId = params.id;
console.log('pathname, ' ,pathname)
  return (
   <html>
    <body>
      {pathname === '/login' ? (
        <LoginHeader />
      ) : (
        <Header />
      )}
      {pathname !=='/login' && !pageId ? (
        <GetHomeRecmList />
      ) : (
        <div>{children}</div>
      )}
      </body>
   </html> 
  );
};

export default RootLayout;