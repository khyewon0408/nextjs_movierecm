"use client"
import React, { useState } from 'react';
import styles from './Login.module.css';
//import getConfig from 'next/config';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const clientId = process.env.NAVER_LOGIN_CLIENTID;
  const redorectUrl = process.env.NVAER_LOGIN_REDIRECT;
  const handleLogin = () => {
    console.log('Username:', username);
    console.log('Password:', password);
    // 로그인 처리 로직 추가
  };

  const handleSignUp = () => {
    console.log('회원가입');
    // 회원가입 처리 로직 추가
  };
  const test = () =>{
    fetch('/api/naverLogin')
  
   }
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>로그인</h2>
      <div className={styles.inputGroup}>
        <input
          type="text"
          placeholder="아이디"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          className={styles.input}
        />
      </div>
      <div className={styles.inputGroup}>
        <input
          type="password"
          placeholder="비밀번호"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className={styles.input}
        />
      </div>
      
      <button onClick={handleLogin} className={styles.button}>로그인</button>
      <button onClick={handleSignUp} className={styles.button}>회원가입</button>
      <a href={`https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=${clientId}&redirect_uri=${redorectUrl}&state=RANDOM_STATE`}>
        <img height="30" src="http://static.nid.naver.com/oauth/small_g_in.PNG"/>
      </a>
    </div>
  );
};

export default Login;