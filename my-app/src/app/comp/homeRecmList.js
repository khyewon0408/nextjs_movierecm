"use client"
import { useState, useEffect } from 'react';
import {  useRouter  } from 'next/navigation';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

export default function getHomeRecmList({ props }) {
    const [slideIndex, setSlideIndex] = useState(0);
    const [imageList, setImageList] = useState([]);
    const [imageWidth, setImageWidth] = useState(185); // 디폴트 이미지 너비
    const [imageHeight, setImageHeight] = useState(278); // 디폴트 이미지 높이
    const router = useRouter();
    useEffect(() => {
        // 이미지 목록을 가져오는 API 호출
        fetchImageList();
    }, []);

    useEffect(() => {
        const interval = setInterval(() => {
            setSlideIndex(prevIndex => (prevIndex + 1) % imageList.length);
        }, 5000); // 이미지 전환 시간 (5초)

        return () => clearInterval(interval);
    }, [imageList]);

    const fetchImageList = async () => {
        const options = {
            method: 'GET',
            headers: {
                accept: 'application/json',
                Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0NzU4YWQ3OTUxZWY5NGI5ZmE0MTc3YWVkYmZjNDk3OSIsInN1YiI6IjY1YzE3ZGI5ZjQ0ZjI3MDE4NGMwMWU0OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HLXkiPgNGR8dkYzKSSAHAKOZcPVWfI_Md67-z1qiZhY'
            }
        };
        const response = await fetch('https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=ko-KR&page=1&sort_by=popularity.desc&with_watch_providers=8', options);

        const data = await response.json();

        setImageList(data.results); // 이미지 목록 설정

        console.log("추천 리스트 :: ", data.results);

        // 이미지 크기 가져오기
        if (data.results.length > 0 && data.results[0].poster_path) {
            const img = new Image();
            img.src = 'https://image.tmdb.org/t/p/w185' + data.results[0].poster_path;
            img.onload = () => {
                setImageWidth(img.width);
                setImageHeight(img.height);
            };
        }
    };

    return (
        <div className='main' style={{ marginLeft: '20px', marginTop:'20px' }}> {/* 여백 추가 */}
            <h1>Movie List</h1>
            <Swiper
                spaceBetween={10}
                slidesPerView={5}
                
            >
                {imageList.map((image, index) => (
                    <SwiperSlide key={index}>
                        <div style={{ float: 'left', width: imageWidth }}>
                            {image.poster_path && (
                                <img src={'https://image.tmdb.org/t/p/w185' + image.poster_path} 
                                alt="Movie Poster" 
                                style={{ width: '100%', height: 'auto' }}
                                onClick={() => {
                                    router.push('/details/' + image.id);}}
                                 />
                            )}
                            <p style={{ wordBreak: 'break-all' }}>{(index + 1) + '. ' + (image.title || '')}</p>
                        </div>
                    </SwiperSlide>
                ))}

            </Swiper>
        </div>
    );
 }