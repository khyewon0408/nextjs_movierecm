// components/Header.js
"use client"
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import styles from './Header.module.css'; // CSS 파일 import

const Header = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const router = useRouter();

  return (
    <header className={styles.header}> {/* CSS 클래스 추가 */}
      <h1 onClick={()=>{router.push('/')}}>타이틀 생각중</h1>
    </header>
  );
};

export default Header;