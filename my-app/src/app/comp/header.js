// components/Header.js
"use client"
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import styles from './Header.module.css'; // CSS 파일 import
import Link from 'next/link';
import Cookies from 'js-cookie';
const Header = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const router = useRouter();

  const myData = Cookies.get('myData');
  console.log('myData ::: ' , myData);
  const handleSearch = () => {
    router.push('/search/' + searchTerm);
  };
  return (
    <header className={styles.header}> {/* CSS 클래스 추가 */}
    
      <h1 onClick={()=>{router.push('/')}}>타이틀 생각중</h1>
      <Link href='/login'><button className={styles.logInBtn} >로그인</button></Link> {/* CSS 클래스 추가 */}
      <div className={styles.searchBox}> {/* CSS 클래스 추가 */}
        <input type='text' id='searchTerm' value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
        <button className={styles.searchbtn} onClick={handleSearch}>search</button> {/* CSS 클래스 추가 */}
      </div>
    </header>
  );
};

export default Header;