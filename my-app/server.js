const express = require('express');
const next = require('next');
const mysql = require('mysql');
const naverLogin = require('./src/app/server/naverLogin');
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

nextApp.prepare().then(() => {
  const app = express();
  const loggerMiddleware = require('./middleware/logger');

  // Express 미들웨어 설정
  app.use(loggerMiddleware);

  // MySQL 연결 설정
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'sys'
  });

  // MySQL 연결
  connection.connect((err) => {
    if (err) {
      console.error('MySQL 연결 실패:', err);
      throw err;
    }
    console.log('MySQL 연결 성공');
  });

  // Express 애플리케이션에 라우팅 추가
 // app.use('/naverlogin', naverLogin);

  // Next.js의 모든 요청을 처리할 핸들러 함수 설정
  app.all('*', (req, res) => {
    return handle(req, res);
  });


  // Express 서버 시작
  const PORT = process.env.PORT || 3001;
  app.listen(PORT, err => {
    if (err) throw err;
    console.log(`Server is listening on port ${PORT}`);
  });


});

