/** @type {import('next').NextConfig} */
const nextConfig = {
    publicRuntimeConfig: {
      NAVER_LOGIN_CLIENTID: process.env.NAVER_LOGIN_CLIENTID,
      NVAER_LOGIN_REDIRECT: process.env.NVAER_LOGIN_REDIRECT
    },
  };
  
  export default nextConfig;
